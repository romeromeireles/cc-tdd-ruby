class Produto
  attr_reader :nome, :valor
  
  def initialize(nome, valor)
    @nome = nome
    @valor = valor
  end

  def ==(other_product)
    return false if self.nome != other_product.nome
    return false if self.valor != other_product.valor

    true
  end
end