require 'test/unit'
require File.expand_path('produto.rb', File.dirname(__FILE__)) 
require File.expand_path('carrinho_de_compras.rb', File.dirname(__FILE__)) 
require File.expand_path('maior_e_menor.rb', File.dirname(__FILE__)) 

class TesteMaiorEMenor < Test::Unit::TestCase

  def teste_ordem_descrescente
    carrinho = CarrinhoDeCompras.new 
    carrinho << Produto.new('Geladeira', 450.0) 
    carrinho << Produto.new('Liquidificador', 250.0) 
    carrinho << Produto.new('Jogo de pratos', 70.0) 

    algoritmo = MaiorEMenor.new 
    algoritmo.encontra carrinho 

    assert_equal 'Jogo de pratos', algoritmo.menor.nome
    assert_equal 'Geladeira', algoritmo.maior.nome
  end

  def teste_unico_produto
    carrinho = CarrinhoDeCompras.new 
    carrinho << Produto.new('Geladeira', 450.0) 

    algoritmo = MaiorEMenor.new
    algoritmo.encontra carrinho

    assert_equal algoritmo.maior, algoritmo.menor
  end
end