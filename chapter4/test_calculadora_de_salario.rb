require 'minitest/autorun'
require_relative 'calculadora_de_salario.rb'
require_relative 'funcionario.rb'

class TestCalculadoraDeSalario < Minitest::Test
  def test_deve_calcular_salario_para_desenvolvedores_com_salario_abaixo_do_limite
    calculadora = CalculadoraDeSalario.new
    funcionario = Funcionario.new('Romero', 1500.0, Cargo::DESENVOLVEDOR)

    salario = calculadora.calcula_salario(funcionario)

    assert_equal(1500 * 0.9, salario)
  end

  def test_deve_calcular_salario_para_desenvolvedores_com_salario_acima_do_limite
    calculadora = CalculadoraDeSalario.new
    funcionario = Funcionario.new('Romero', 4000.0, Cargo::DESENVOLVEDOR)

    salario = calculadora.calcula_salario(funcionario)

    assert_equal(4000 * 0.8, salario)
  end

  def test_deve_calcular_salario_para_DBAs_com_salario_acima_do_limite
    calculadora = CalculadoraDeSalario.new
    funcionario = Funcionario.new('Romero', 2700.0, Cargo::DBA)

    salario = calculadora.calcula_salario(funcionario)

    assert_equal(2700.0 * 0.75, salario)
  end

  def test_deve_calcular_salario_para_DBAs_com_salario_abaixo_do_limite
    calculadora = CalculadoraDeSalario.new
    funcionario = Funcionario.new('Romero', 500.0, Cargo::DBA)

    salario = calculadora.calcula_salario(funcionario)

    assert_equal(500 * 0.85, salario)
  end
end