require 'minitest/autorun'
require_relative 'conversor_de_numero_romano.rb'

class ConversorDeNumeroRomanoTeste < MiniTest::Test

  def test_deve_entender_unico_simbolo
    romano = ConversorDeNumeroRomano.new
    numero = romano.converte 'V'
    assert_equal 5, numero
  end

  def test_deve_entender_dois_simbolos
    romano = ConversorDeNumeroRomano.new
    numero = romano.converte 'II'
    assert_equal 2, numero
  end

  def test_deve_entender_quatro_simbolos_dois_a_dois
    romano = ConversorDeNumeroRomano.new
    numero = romano.converte 'XXII'
    assert_equal 22, numero
  end

  def test_deve_entender_subtracoes
    romano = ConversorDeNumeroRomano.new
    numero = romano.converte 'IX'
    assert_equal 9, numero
  end

  def test_deve_entender_numeros_complexos
    romano = ConversorDeNumeroRomano.new
    numero = romano.converte 'XXIV'
    assert_equal 24, numero
  end
end