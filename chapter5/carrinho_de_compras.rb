class CarrinhoDeCompras
  attr_reader :itens

  def initialize
    @itens = []
  end

  def <<(item)
    itens << item
  end

  def maior_valor
    # return 0 if self.itens.empty?
    # return self.itens.first.valor_total if self.itens.length == 1

    # maior = self.itens.first.valor_total
    # self.itens.each do |i|
    #   maior = i.valor_total if i.valor_total > maior
    # end

    self.itens.map(&:valor_total).max || 0
  end
end