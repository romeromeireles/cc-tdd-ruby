class CarrinhoDeComprasBuilder
  def initialize
    @carrinho = CarrinhoDeCompras.new
  end

  def com_items(*valores)
    valores.each do |valor|
      @carrinho << Item.new('item', 1, valor)
    end
    self
  end

  def cria
    @carrinho
  end
end