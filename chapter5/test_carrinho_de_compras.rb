require 'minitest/autorun'
require_relative 'carrinho_de_compras.rb'
require_relative 'item.rb'

class TestCarrinhoDeCompras < Minitest::Test
  def setup
    @carrinho = CarrinhoDeCompras.new

    assert_empty @carrinho.itens
  end

  def test_retorna_zero_se_vazio
    assert_empty @carrinho.itens
    assert_equal 0, @carrinho.maior_valor
  end

  def test_retorna_valor_do_item_se_houver_apenas_um
    geladeira = Item.new('Geladeira', 1, 900.0)

    @carrinho << geladeira

    assert_equal 1, @carrinho.itens.size
    assert_equal geladeira.valor_total, @carrinho.maior_valor
  end

  def test_retorna_maior_valor_dentre_varios_itens
    geladeira = Item.new('Geladeira', 3, 1500.0)
    fogao = Item.new('Fogão DAKO', 1, 2400.0)
    tv = Item.new('TV Samsung', 2, 1800.0)

    @carrinho << geladeira
    @carrinho << fogao
    @carrinho << tv

    assert @carrinho.itens.size > 1
    assert_equal 4500.0, @carrinho.maior_valor
  end
end